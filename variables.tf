variable "allocated_storage" {
  type        = number
  description = "The allocated storage in gibibytes"
}

variable "storage_type" {
  type        = string
  description = "The default is io1 if iops is specified, gp2 if not"
}

variable "engine" {
  type        = string
  description = "The database engine to use"
}

variable "backup_retention_period" {
  type        = number
  description = "backup retention period in days"
}

variable "engine_version" {
  type        = string
  description = "The engine version to use"
}

variable "instance_class" {
  type        = string
  description = "The instance type of the RDS instance"
}

variable "name" {
  type        = string
  description = "The name of the database to create when the DB instance is created"
  default     = null
}

variable "identifier" {
  type        = string
  description = "The name of the RDS instance, if omitted, Terraform will assign a random, unique identifier"
}

variable "username" {
  type        = string
  description = "Username for the master DB user"
}

variable "password" {
  type        = string
  description = "Password for the master DB user"
}

variable "skip_final_snapshot" {
  type        = bool
  description = "Determines whether a final DB snapshot is created before the DB instance is deleted"
  default     = true
}

variable "publicly_accessible" {
  type        = bool
  description = "publicly accessible to connect from Local computer"
  default     = true
}

variable "vpc_security_group_ids" {
  type        = string
  description = "List of VPC security groups to associate"
}

variable "subnet_ids" {
  type        = list(string)
  description = "subnets where the DB  will be available to"
}

variable "env" {
  type = string
}