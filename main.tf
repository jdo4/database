resource "aws_db_subnet_group" "sqldb_subnet_group" {
  name       = "${var.env}-workers_subnets"
  subnet_ids = var.subnet_ids

  tags = {
    Name = "${var.env}-sqldb-subnet-group"
    Env  = var.env
  }
}

resource "aws_db_instance" "sqldb" {
  backup_retention_period = var.backup_retention_period
  db_subnet_group_name   = aws_db_subnet_group.sqldb_subnet_group.id
  allocated_storage      = var.allocated_storage
  storage_type           = var.storage_type
  engine                 = var.engine
  engine_version         = var.engine_version
  instance_class         = var.instance_class
  name                   = var.name
  identifier             = var.identifier        
  username               = var.username
  password               = var.password
  skip_final_snapshot    = var.skip_final_snapshot   
  vpc_security_group_ids = [var.vpc_security_group_ids]
  publicly_accessible    = var.publicly_accessible

  tags = {
    Name = "${var.env}-sqlserver"
    Env  = var.env
  }
}